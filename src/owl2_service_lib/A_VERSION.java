package owl2_service_lib;

/**
 * SERVICE_LIB
 */
public class A_VERSION {

  final static public String app = "owl2_service_lib";
  final static public int version = 1;
  final static public int owl2_lib_min = 1;

  /*
   * 25/10/2020 1.1
   * inicial
   *
   */
  static private boolean done;

  static public void check(final String checker, final int minimum){
    if(!done){
      done = true;
      System.out.println(app+".version="+version);
    }
    if(version<minimum){
      System.err.println(app+".version needed="+minimum+" ["+checker+"]");
      System.exit(1);
    }
    owl2_lib.A_VERSION.check(app, owl2_lib_min);
  }

}
